ifneq ($(KERNELRELEASE),)
obj-m := kbench_mod.o
else
CC := gcc
CFLAGS := -Wall -O2 -I/usr/local -L/usr/local
LDFLAGS := -lmnl

KDIR ?= /lib/modules/`uname -r`/build

all: route_bench udpflood kbench_mod

route_bench: route_bench.c

kbench_mod: kbench_mod.c
	$(MAKE) -C $(KDIR) M=$$PWD

udpflood: udpflood.c

clean:
	rm route_bench udpflood kbench_mod
endif
